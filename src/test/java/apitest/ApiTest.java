package apitest;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import org.json.JSONArray;
import org.json.JSONObject;

import org.junit.Test;

import static testsupport.TestSupport.createRequest;
import static testsupport.TestSupport.isBeerInArray;


public class ApiTest {

    /**
     * Tests if Single beer is returned with appropriate api parameters
     * @throws Exception if request is invalid or there is a connection problem
     */
    @Test
    public void testIfSingleBeerReturnsOneBeer() throws Exception{
        int expectedArrayLength = 1;
        String parameter = "/1";

        int res = new JSONArray(createRequest(parameter)).length();
        assertEquals(expectedArrayLength , res);
    }

    /**
     * Tests if beer named Arcade Nation is returned with correct request parameters
     * @throws Exception if request is invalid or there is a connection problem
     */
    @Test
    public void testIfBeerWithCorrectNameIsReturned() throws Exception {
        String expectedBeerName = "Arcade Nation";
        String parameter = "?beer_name=Arcade_Nation";
        String property = "name";

        JSONArray jsonArray =  new JSONArray(createRequest(parameter));
        assertTrue(isBeerInArray(jsonArray,expectedBeerName,property));
    }


    /**
     * Tests if error 400 is shown after empty page parameter is used
     * @throws Exception if request is invalid or there is a connection problem
     */
    @Test
    public void testIfError400IsReturnedWithoutCorrectParameter() throws Exception{
        int expectedStatusCode = 400;
        String parameter = "?page=";

        Object res = new JSONObject(createRequest(parameter)).get("statusCode");
        assertEquals(expectedStatusCode , res);
    }
}

