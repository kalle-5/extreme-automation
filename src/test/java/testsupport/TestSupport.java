package testsupport;

import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpUriRequest;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.IOException;

public class TestSupport {

    private static final String PUNK_URL = "https://api.punkapi.com/v2/beers";

    /**
     * Handles api requests and returns the response
     * @param parameter is a request parameter that is added to the main api URI.
     * @return JSON response in String
     * @throws IOException in case of a problem or the connection was aborted
     */
    public static String createRequest(String parameter)throws IOException{
        HttpClient httpClient = HttpClientBuilder.create().build();
        HttpUriRequest request = new HttpGet(PUNK_URL+parameter);
        return EntityUtils.toString(httpClient.execute(request).getEntity(), "UTF-8");
    }

    /**
     * Searches JSON Array for a JSON Object with expected property content.
     * @param jsonArray input JSON Array
     * @param expectedResult The string that is searched for in the array
     * @param property JSON property that used for testing
     * @return True if Correct Content was found in the JSON Array
     * @throws JSONException If the JSON Array is not valid.
     */
    public static boolean isBeerInArray(JSONArray jsonArray, String expectedResult, String property)throws JSONException {
        boolean isBeerInArray = false;
        for (int i = 0; i < jsonArray.length(); i++) {
            Object res = jsonArray.getJSONObject(i).get(property);
            if (expectedResult.equals(res)) {
                isBeerInArray = true;
                break;
            }
        }
        return isBeerInArray;
    }


}
