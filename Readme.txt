What is this project?

This is a project for FOB solutions internal automation course.

What software is used to create this?

Gradle, Java (version 8), Intellij IDEA (2017.1 EAP Build #IC-171.3019.7), JUnit (4.11)

How to run the API tests?

1. Clone the project from repository
2. Open the Command prompt or Terminal and enter the project directory
3. Run the command: gradlew test

Individual results are shown for the tests.

How to run the tests again?
1. Run command: gradlew clean
2. Now you can use the command "gradlew test" to run the tests again.

